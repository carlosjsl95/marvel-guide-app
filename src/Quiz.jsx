import React, { Component } from "react";
import quizQuestions from './services/quiz';
import { Card, CardWrapper } from 'react-swipeable-cards';
import './Login.css';
import MyEndCard from './MyEndCard';

export default class Quiz extends Component {
  state = {
    questions: quizQuestions,
    userPoints: 0,
  }

  onSwipe = (element) => {
    let {questions } = this.state;
      this.setState({
        questions: questions.filter((question) => question.id !== element.id),
    });
  }

  getEndCard = () => {
    return(
      <MyEndCard
        userPoints={this.state.userPoints}
        history={this.props.history}
      />
    );
  }

  setUserAnswer = (answer, element) => {
    let {questions} = this.state;
    if (quizQuestions[element.id][answer] === quizQuestions[element.id].correctAnswer) {
      this.setState(prevState=>({
        userPoints: prevState.userPoints + 10,
      }));
    } 
      this.setState({
        questions: questions.filter((question) => question.id !== element.id),
    });
  }

  renderCards() {
    return this.state.questions.map(element => {
      return(
        <Card
          key={element.id}
          onSwipe={() => this.onSwipe(element)}
        >
            <div className="card__container" style={{pointerEvents: 'none', backgroundImage: `url(${element.cardImage})`}}>
              <div style={{backgroundColor: 'rgba(0, 0, 0, 0.6)', alignItems: 'center', justifyContent: 'center', display: 'flex', marginTop: '80px'}}>
                <p className="card__questions__title">{element.question}</p>
              </div>
              <div className="card__questions-wrapper">
                <div className="card__questions" onClick={() => this.setUserAnswer('firstAlternative', element)}>
                  <p className="card__questions__text">{element.firstAlternative}</p>
                </div>
                <div className="card__questions"  onClick={() => this.setUserAnswer('secondAlternative', element)}>
                  <p className="card__questions__text">{element.secondAlternative}</p>
                </div>
                <div className="card__questions"  onClick={() => this.setUserAnswer('thirdAlternative', element)}>
                  <p className="card__questions__text">{element.thirdAlternative}</p>
                </div>
                <div className="card__questions"  onClick={() => this.setUserAnswer('fourthAlternative', element)}>
                  <p className="card__questions__text">{element.fourthAlternative}</p>
                </div>
              </div>

            </div>
        </Card>
      );
    });
  }
  
  render() {
    return(
      <CardWrapper addEndCard={() => this.getEndCard()}>
        {this.renderCards()}
      </CardWrapper>
    );
  }
}