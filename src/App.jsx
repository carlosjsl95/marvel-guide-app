import React from 'react';
import Login from './Login';
import Quiz from './Quiz';
import { Switch, Route, HashRouter } from 'react-router-dom';
import './App.css';

function App() {
  return (
    <HashRouter>
      <Switch>
          <Route exact path='/' component={Login}/>
          <Route exact path='/quiz' component={Quiz}/>
      </Switch>
    </HashRouter>
  );
}

export default App;
