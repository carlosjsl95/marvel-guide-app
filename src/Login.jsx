import React, { Component } from 'react';
import {
  Modal,
} from 'semantic-ui-react';
import marvelService from './services/marvel';
import './Login.css';
import ReactTable from 'react-table';

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {  
      data: [],
      columns: [],
      loading: false,
      showModal: false,
    };
  }

  setComics = (comics) => {
    this.setState({
      data:  comics,
      columns: [{
        Header: 'Título',
        accessor: 'title'
      },
      {
        Header: 'Preço',
        accessor: 'prices',
        Cell: props => <span> {`U$ ${props.original.prices[0].price}`}</span>,
        width: 100,
      },
      {
        Header: 'Capa',
        accessor: 'thumbnail',
        Cell: props => <img className="comic-image" src={`${props.original.thumbnail.path}.${props.original.thumbnail.extension}`}  alt="comics"/>,
        width: 100,
      },
      {
        Header: 'Resumo',
        accessor: 'textObjects',  
        Cell: props => <span> {props.original.textObjects.length > 0  ? `${props.original.textObjects[0].text}` : 'No resume'}</span>,
      }]
    });
  }

  setCharacters= (comics) => {
    this.setState({
      data:  comics,
      columns: [{
        Header: 'Nome',
        accessor: 'name',
        Cell: props => <span style={{fontSize: 40}}> {props.original.name}</span>,
      },
      {
        Header: 'Capa',
        accessor: 'thumbnail',
        Cell: props => <img className="comic-image comic-image--large" src={`${props.original.thumbnail.path}.${props.original.thumbnail.extension}`}  alt="comics"/>,
        width: 300,
      }]
    });
  }

  setStories= (comics) => {
    this.setState({
      data:  comics,
      columns: [{
        Header: 'Storie',
        accessor: 'title',
      },
      {
        Header: 'Comic',
        accessor: 'originalIssue',
        Cell: props => <span>{props.original.originalIssue.name}</span>,
        width: 300,
      }]
    });
  }

  loadSectionData = async (sectionName) => {
    try {
      this.setState({loading: true});
      if(sectionName === 'comicsModal') {
        const { data } = await marvelService.getComics();
        this.setState({loading: false});
        const comics = data.data.results;
        this.setComics(comics);
      }

      if(sectionName === 'charactersModal') {
        const { data } = await marvelService.getCharacters();
        this.setState({loading: false});
        const characters = data.data.results;
        this.setCharacters(characters);
      }

      if(sectionName === 'storiesModal') {
        const { data } = await marvelService.getStories();
        this.setState({loading: false});
        const stories = data.data.results;
        this.setStories(stories);
      }

      if(sectionName === 'seriesModal') {
        const { data } = await marvelService.getSeries();
        this.setState({loading: false});
        const series = data.data.results;
        this.setStories(series);
      }
    } catch (error) {
      console.log(error.message);
      this.setState({loading: false});
    }
  }

  handleOpen = (sectionName) => {
    this.setState({ showModal: true });
    this.loadSectionData(sectionName);
  }

  handleClose = () => this.setState({ showModal: false });

  render() {
    return (
      <div className="container">
        <div className="wrapper">
          <div className="section" onClick={() => this.handleOpen('comicsModal')}>
            <p className="section__title">COMICS</p>
          </div>
          <div className="section" onClick={() => this.handleOpen('charactersModal')}>
            <p className="section__title--small">CHARACTERS</p>
          </div>
          <div className="section" onClick={() => this.handleOpen('storiesModal')}>
            <p className="section__title">STORIES</p>
          </div>
          <div className="section" onClick={() => this.handleOpen('seriesModal')}>
            <p className="section__title">SERIES</p>
          </div>
          <div className="section--big" onClick={() => this.props.history.push('/quiz')}>
            <p className="section__title--small">TESTE SEUS CONHECIMENTOS</p>
          </div>
        </div>
        <Modal
          open={this.state.showModal}
          onClose={() => this.handleClose()}
          centered={false}
          style={{backgroundColor: 'black'}}
        >
          <Modal.Header>MARVEL</Modal.Header>
          <Modal.Content>
            {
              this.state.loading ? 
              'Carregando..'
              : <ReactTable
                  data={this.state.data}
                  columns={this.state.columns}
                />
            }
          </Modal.Content>
        </Modal>
      </div>
    );
  }
}
