export const quiz = [
  {
    id: 0,
    question: 'Qual é o nome do primeiro amor de Wolverine?',
    firstAlternative: 'Mary Jane',
    secondAlternative: 'Rose O’Hara',
    thirdAlternative: 'Emma Frost',
    fourthAlternative: 'Jean Grey',
    correctAnswer: 'Rose O’Hara',
    cardImage: 'https://i.pinimg.com/originals/81/e3/1b/81e31b102531ebf19d3b67319846e1ee.jpg',
  },
  {
    id: 1,
    question: 'Quem é responsável pelo programa do soro do super soldado no universo Ultimate da Marvel?',
    firstAlternative: 'Tony Stark',
    secondAlternative: 'Bruce Banner',
    thirdAlternative: 'Hank Pym',
    fourthAlternative: 'Nick Fury',
    correctAnswer: 'Hank Pym',
    cardImage: 'https://i.pinimg.com/736x/bc/cd/b5/bccdb536ede2e6bb8473edf6c86e939e.jpg',
  },
  {
    id: 2,
    question: 'Quem é trazido de volta a vida em Surpreendentes X-Men?',
    firstAlternative: 'Jean Grey',
    secondAlternative: 'Noturno',
    thirdAlternative: 'Colossus',
    fourthAlternative: 'Professor X',
    correctAnswer: 'Colossus',
    cardImage: 'https://pbs.twimg.com/profile_images/1128378103418384384/m93sMuwS.png',
  },
  {
    id: 3,
    question: 'Como Loki retorna após os eventos do Ragnarok?',
    firstAlternative: 'No corpo do Odin',
    secondAlternative: 'Na sua forma original',
    thirdAlternative: 'No corpo de um sapo',
    fourthAlternative: 'No corpo de uma mulher',
    correctAnswer: 'No corpo de uma mulher',
    cardImage: 'https://i.pinimg.com/originals/43/d7/d2/43d7d2333f36d2a5a672e71d07cfaab9.jpg',
  },
  {
    id: 4,
    question: 'Quem consegue fazer frente ao Hulk na saga Hulk contra o Mundo?',
    firstAlternative: 'Coisa',
    secondAlternative: 'Homem de Ferro',
    thirdAlternative: 'Thor',
    fourthAlternative: 'Sentinela',
    correctAnswer: 'Sentinela',
    cardImage: 'https://i.pinimg.com/originals/b0/1d/54/b01d54dce76ef6bcf36e0e04511c1807.jpg'
  },
  {
    id: 5,
    question: 'Qual é o objetivo do Thanos ao reunir as Jóias do Infinito?',
    firstAlternative: 'Se vingar dos heróis',
    secondAlternative: 'Matar metade dos seres do universo',
    thirdAlternative: 'Se tornar imortal',
    fourthAlternative: 'Se tornar o líder supremo do universo',
    correctAnswer: 'Matar metade dos seres do universo',
    cardImage: 'https://i.pinimg.com/originals/74/bd/14/74bd14fb5752b4f797e3f6177f9c73ad.jpg'
  },
  {
    id: 6,
    question: 'Quem é o traidor infiltrado no time do Capitão América na saga Guerra Civil?',
    firstAlternative: 'Miss Marvel',
    secondAlternative: 'Tigresa',
    thirdAlternative: 'Justiceiro',
    fourthAlternative: 'Luke Cage',
    correctAnswer: 'Tigresa',
    cardImage: 'https://i.pinimg.com/originals/b0/1d/54/b01d54dce76ef6bcf36e0e04511c1807.jpg'
  },
  {
    id: 7,
    question: 'Todos sabemos que Capitão América é o primeiro vingador e seu filme é o primeiro na linha cronológica né!? Qual o projeto que deu poderes a Steve Rogers?',
    firstAlternative: 'Arma X',
    secondAlternative: 'Exército moderno',
    thirdAlternative: 'Projeto vingadores',
    fourthAlternative: 'Soro do super soldado',
    correctAnswer: 'Soro do super soldado',
    cardImage: 'https://i.pinimg.com/736x/bc/cd/b5/bccdb536ede2e6bb8473edf6c86e939e.jpg'
  },
  {
    id: 8,
    question: 'No filme o melhor amigo de Capitão morre, qual era seu verdadeiro nome?',
    firstAlternative: 'Bucky',
    secondAlternative: 'James Buchanan “Bucky” Barnes',
    thirdAlternative: 'Coronel Philips',
    fourthAlternative: 'John Doe',
    correctAnswer: 'James Buchanan “Bucky” Barnes',
    cardImage: 'https://i.pinimg.com/736x/bc/cd/b5/bccdb536ede2e6bb8473edf6c86e939e.jpg'
  }
];

export default quiz;