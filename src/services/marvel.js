import axios, { queryParams } from './index';

const getComics = () => axios.get(`/comics?ts=${queryParams().ts}&apikey=${queryParams().apikey}&hash=${queryParams().hash}`);
const getCharacters = () => axios.get(`/characters?ts=${queryParams().ts}&apikey=${queryParams().apikey}&hash=${queryParams().hash}`);
const getStories = () => axios.get(`/stories?ts=${queryParams().ts}&apikey=${queryParams().apikey}&hash=${queryParams().hash}`);
const getSeries = () => axios.get(`/series?ts=${queryParams().ts}&apikey=${queryParams().apikey}&hash=${queryParams().hash}`);

export default {
  getComics,
  getCharacters,
  getStories,
  getSeries,
};
