import React from 'react';
import './Login.css';

export default function MyEndCard(props) {
  return (
    <div className="card__container--center" style={{pointerEvents: 'none'}}>
      <p className="card__questions__title__end-card">
        { props.userPoints > 30 ? 'Parabéns!' : 'Que pena!' }
      </p>
      <p className="card__questions__title__end-card">
        Sua pontuação foi <br></br>
        {props.userPoints}
      </p>
      <button 
        className="card__back-button"
        onClick={() => props.history.push('/')}
        style={{
          pointerEvents: 'all',
          cursor: 'pointer'
        }}
      >Voltar</button>
    </div>
  );
}
